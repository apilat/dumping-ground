FROM rust:slim-buster
RUN apt-get update && apt-get install libsqlite3-dev -y

EXPOSE 8000

WORKDIR /app
COPY Cargo.lock .
COPY Cargo.toml .
RUN mkdir -p .cargo src
RUN echo "fn main() {}" > src/main.rs
RUN cargo build --release

COPY ./src src
RUN cargo build --release
RUN cargo install --path .
COPY ./templates templates
COPY ./static static

ENV ROCKET_ADDRESS=0.0.0.0
CMD ["knowledge-base"]
